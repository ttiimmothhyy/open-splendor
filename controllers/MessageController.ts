import {MessageService} from "../services/MessageService";
import {Request,Response} from "express";
import {io} from "../main";
import {logger} from "../logger";

export class MessageController{
	constructor(private messageService:MessageService){}

	transmission = async(req:Request,res:Response) => {
		try{
			const data = req.body;
			const transmission = JSON.stringify(data);
			const uuId = req.session["uuId"];
			await this.messageService.transmission(transmission,uuId);
			io.to(uuId).emit('transmission-received', data);
			res.json({success:true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	guestMessage = async(req:Request,res:Response) => {
		try{
			const message = req.body;
			const userFound = req.session["user"];
			io.emit('global-chatroom',`${userFound?.name}: ${message.content}`);
			await this.messageService.guestMessage(userFound.name,message.content);
			res.json({success:true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	userMessage = async(req:Request,res:Response) => {
		try{
			const message = req.body;
			const userFound = req.session["user"];
			io.to('user').emit('user-chatroom',`${userFound?.name}: ${message.content}`);

			if(req.session["loggedIn"]){
				await this.messageService.userMessage(userFound.name,message.content)
			}
			res.json({success:true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	roomMessage = async(req:Request,res:Response) => {
		try{
			const message = req.body;
			const userFound = req.session["user"];
			const uuId = req.session["uuId"];
			io.to(uuId).emit('room-chatroom', `${userFound?.name}: ${message.content}`);
			if (req.session["uuId"]) {
				await this.messageService.roomMessage(req.session["uuId"],userFound.name,message.content)
			}
			res.json({success:true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	friendMessage = async(req:Request,res:Response) => {
		try{
			const message = req.body;
			const userFound = req.session["user"];
			io.to(`friend-${req.session["user"].name}`).emit('friend-chatroom',{message:`${userFound.name}: ${message.content}`,
			name:userFound.name,content:message.content});

			res.json({success:true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	loadChatGlobal = async(req: Request, res: Response) => {
		try{
			const message = await this.messageService.loadChatGlobal();
			res.json(message);
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	loadChatUser =  async (req: Request, res: Response) => {
		try{
			const message = await this.messageService.loadChatUser()
			res.json(message);
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	loadChatRoom = async(req:Request,res:Response) => {
		try{
			// console.log(req.session["uuId"])
			if(req.session["uuId"]){
				const message = await this.messageService.loadChatRoom(req.session["uuId"])
				res.json(message);
			}else{
				res.json({success: true});
			}
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	loadChatFriend = async(req:Request,res:Response) => {
		try{
			const message = await this.messageService.loadChatFriend(req.session["user"].name)
			res.json(message);
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	storeChatFriend = async(req:Request,res:Response) => {
		try{
			const message = req.body;
			await this.messageService.storeChatFriend(message.name,message.content,message.belong)
			res.json({success:true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	storeVoiceFriend = async(req:Request,res:Response) => {
		try{
			const message = req.body;
			if(req.file){
				message.content = `/uploads/${req.file.filename}`;
			}
			await this.messageService.storeVoiceFriend(message.name,message.content,message.belong)
			res.json({success: true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}
}