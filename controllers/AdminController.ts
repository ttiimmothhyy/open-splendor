import {AdminService} from "../services/AdminService";
import {Request,Response} from "express";
import {logger} from "../logger";

export class AdminController{
	constructor(private adminService:AdminService){}

	addQuestion = async(req:Request,res:Response) => {
		try{
			const question = req.body;
			await this.adminService.addQuestion(req.session["user"].name,question.content)
			res.json({success:true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	loadQuestion = async(req:Request,res:Response) => {
		try{
			if(req.session["user"].name !== 'admin'){
				const question = await this.adminService.loadUserQuestion(req.session["user"].name)
				res.json(question);
			}else{
				const question = await this.adminService.loadAdminQuestion()
				res.json(question);
			}
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	answerQuestion = async(req:Request,res:Response) => {
		try{
			const question = req.body;
			await this.adminService.answerQuestion(question.content,question.question)
			res.json({success: true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}
}