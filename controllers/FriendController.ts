import {FriendService} from "../services/FriendService";
import {Request,Response} from "express";
import {logger} from "../logger";

export class FriendController{
	constructor(private friendService:FriendService){}

	addFriend = async(req:Request,res:Response) => {
		try{
			const friend = await this.friendService.getFriendId(req.body.name)
			const userId = await this.friendService.getUserId(req.session["user"].name)
			const friendList = await this.friendService.checkFriend(userId.id,friend.id)

			if(!friendList && !userId.name.includes('Guest')){
				await this.friendService.addFriend(userId.id,friend.id)
			}
			res.json({success: true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	removeFriend = async(req:Request,res:Response) => {
		try{
			const friend = await this.friendService.getFriendId(req.body.name)
			const userId = await this.friendService.getUserId(req.session["user"].name)
			await this.friendService.removeFriend(userId.id,friend.id)
			res.json({success: true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	loadFriend = async(req:Request,res:Response) => {
		try{
			const userId = await this.friendService.getUserId(req.session["user"].name)
			const friend = await this.friendService.loadFriend(userId.id)
			res.json(friend);
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}
}