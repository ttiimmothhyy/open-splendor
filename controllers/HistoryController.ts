import {HistoryService} from "../services/HistoryService";
import {Request,Response} from "express";
import {logger} from "../logger";
import {Result} from "../services/models";

export class HistoryController{
	constructor(private historyService:HistoryService){}

	history = async(req:Request,res:Response) => {
		try{
			const username = req.body.name;
			// const username = req.session["user"].name;
			const hasHistory = await this.historyService.hasHistory(username)
			if (hasHistory !== 0) {
				const userHistory = await this.historyService.userHistory(username);
				res.json({history:userHistory,username});
			} else {
				res.json({message:'no history'});
			}
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	result = async(req:Request,res:Response) => {
		try{
			const data = req.body;
			const uuId = req.session["uuId"];
			const winner = data.result.reduce((previous:Result,current:Result) => {
					if (current.score > previous.score) {
						return current;
					}
					return previous;
				},
				{name:'winner',score:0}
			)
			const username = req.session["user"].name;

			const timeSpent = data.timeSpent;
			const resultArray = JSON.stringify(data.result);
			// const videoName = data.videoName;
			const numOfPlayers = await this.historyService.getNumberOfPlayers(uuId);
			const currentTurn = await this.historyService.currentTurn(uuId);
			await this.historyService.updateHistory(uuId,winner.name,resultArray,timeSpent,numOfPlayers,currentTurn)
			await this.historyService.updateMapping(uuId,username)
			res.json({success:true});
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}

	allHistory = async(req:Request,res:Response) => {
		try{
			const hasHistory = await this.historyService.totalHistory();
			if (hasHistory !== 0) {
				const history = await this.historyService.history()
				const users = await this.historyService.users()
				res.json({history,user:users});
			} else {
				res.json({message:"no history"});
			}
		}catch(e){
			logger.error(`path:${req.path} method:${req.method}`);
			logger.error(e)
			res.status(500).json({success:false,message:e.toString()})
		}
	}
}