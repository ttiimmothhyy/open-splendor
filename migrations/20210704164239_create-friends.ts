import {Knex} from "knex";

export async function up(knex:Knex):Promise<void>{
	await knex.schema.createTable("friends",(table) => {
		table.increments();
		table.integer("user_id").unsigned();
		table.foreign("user_id").references("users.id");
		table.integer("another_user_id");
		table.foreign("another_user_id").references("users.id");
	})
}

export async function down(knex:Knex):Promise<void>{
	await knex.schema.dropTable("friends");
}