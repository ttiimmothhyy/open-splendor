let currentUser;
let socket;
let warningButton = document.querySelector(".show-unmatch");
let wrongmessage = document.querySelector(".show-unmatch-password");
let sameusername = document.querySelector(".show-used");
let sameEmail = document.querySelector(".show-used-email");
let signupSection = document.querySelector(".signup");
let messagePanel1 = document.querySelector(".message-area-1");
let messagePanel2 = document.querySelector(".message-area-2");
let messagePanel3 = document.querySelector(".message-area-3");
let messagePanel4 = document.querySelector(".message-area-4");
let roomGlobal = document.querySelector(".global");
let roomUser = document.querySelector(".user");
let roomRoom = document.querySelector(".room");
let roomPrivate = document.querySelector(".private");
let messageForm1 = document.querySelector("#message-form-1");
let messageForm2 = document.querySelector("#message-form-2");
let messageForm3 = document.querySelector("#message-form-3");
let messageForm4 = document.querySelector("#message-form-4");
let gameRoom = document.querySelector(".gameroom-temp");
let forgetPasswordButton = document.querySelector(".forget-password");
let splendor = document.querySelector(".splendor");
let sevenwonder = document.querySelector(".seven-wonder");
let carcassonne = document.querySelector(".carcassonne");
let fullDisplay = document.querySelector(".full-display");
let secondFullDisplay = document.querySelector(".second-full-display");
let thirdFullDisplay = document.querySelector(".third-full-display");
let cross = document.querySelector(".imageflex");
let secondCross = document.querySelector(".second-imageflex");
let thirdCross = document.querySelector(".third-imageflex");
let userLogin = document.querySelector(".userplay");
let signupButton = document.querySelector(".signup");
let icon = document.querySelector(".circle");
let hiddenUpload = document.querySelector(".hidden-upload");
let signupForm = document.querySelector("#signup-form");
let clickPanel = document.querySelector(".click")
let toLoginPage = document.querySelector(".tologinpage")
let secondToLoginPage = document.querySelector(".second-tologinpage")
let sendPasswordEmail = document.querySelector(".send-password-email");
let email = document.querySelector(".email-input");
let confirmButton = document.querySelector(".confirm-send");
let firstpassword = document.querySelector(".passwordinput-firstpassword")
let password = document.querySelector(".passwordinput-password");
let username = document.querySelector(".passwordinput-username");
let emailSignup = document.querySelector(".passwordinput-email");
let guestLogin = document.querySelector(".guest");

document.querySelector('#loginform').addEventListener('submit',async(event)=>{
    event.preventDefault(); // To prevent the form from submitting synchronously
   
    const form = event.currentTarget;
    const user = {};

    user.username = form.username.value;
    user.password = form.password.value;

    // create your form object with the form inputs
    const res = await fetch('/login',{
        method:"post",
        headers:{
            "Content-Type":"application/json"
        },
        body:JSON.stringify(user)
    });

    if(res.status === 200){
        const result = await res.json(); // String -> Object
        if(result.success && result.name === "admin"){
            window.location = "/admin.html";
        }else if(result.success){
            window.location = "/lobby.html";
        }
    }else{
        document.querySelector(".show-wrong-message").innerHTML = `Incorrect Username/Password`
        
    }
});

splendor.addEventListener('mousemove',function(event){
    event.currentTarget.innerText = "Splendor";
})

splendor.addEventListener('mouseleave',function(event){
    event.currentTarget.innerText = "";
})
splendor.addEventListener('click',function(){
    if(!(secondFullDisplay.classList.contains("show-second-full-display") || thirdFullDisplay.classList.contains("show-third-full-display"))){
        fullDisplay.classList.add("show-full-display");
        userLogin.innerText = "Splendor: User Login"
        clickPanel.innerHTML = ""
    }   
})

sevenwonder.addEventListener('mousemove',function(event){
    event.currentTarget.innerText = "Seven Wonder";
})
sevenwonder.addEventListener('mouseleave',function(event){
    event.currentTarget.innerText = "";
})
sevenwonder.addEventListener('click',function(){
    if(!(secondFullDisplay.classList.contains("show-second-full-display") || thirdFullDisplay.classList.contains("show-third-full-display"))){
        fullDisplay.classList.add("show-full-display");
        userLogin.innerText = "Seven Wonder: User Login"
        clickPanel.innerHTML = ""
    }
})

carcassonne.addEventListener('mousemove',function(event){
    event.currentTarget.innerText = "Carcassonne";
})
carcassonne.addEventListener('mouseleave',function(event){
    event.currentTarget.innerText = "";
})
carcassonne.addEventListener('click',function(){
    if(!(secondFullDisplay.classList.contains("show-second-full-display") || thirdFullDisplay.classList.contains("show-third-full-display"))){
        fullDisplay.classList.add("show-full-display");
        userLogin.innerText = "Carcassonne: User Login"
        clickPanel.innerHTML = ""
    }
})

signupButton.addEventListener('click',function(){
    fullDisplay.classList.remove("show-full-display");
    secondFullDisplay.classList.add("show-second-full-display")
})
forgetPasswordButton.addEventListener('click',function(){
    fullDisplay.classList.remove("show-full-display");
    thirdFullDisplay.classList.add("show-third-full-display")
})
cross.addEventListener('click',function(){
    fullDisplay.classList.remove("show-full-display");
    clickPanel.innerHTML = "Click on the game you want to play";
    document.querySelector(".show-wrong-message").innerHTML = "";
})
secondCross.addEventListener('click',function(){
    secondFullDisplay.classList.remove("show-second-full-display");
    clickPanel.innerHTML = "Click on the game you want to play"
    warningButton.innerHTML = ""
    wrongmessage.innerText = ""
    sameusername.innerText = ""
    icon.innerHTML = `<input type="button" value="Upload Icon" onclick="document.querySelector('#selectedFile').click()">`
})
thirdCross.addEventListener('click',function(){
    thirdFullDisplay.classList.remove("show-third-full-display");
    clickPanel.innerHTML = "Click on the game you want to play"
    confirmButton.innerHTML = ""
})
    
toLoginPage.addEventListener('click',function(){
    fullDisplay.classList.add("show-full-display");
    secondFullDisplay.classList.remove("show-second-full-display");
    warningButton.innerHTML = ""
    wrongmessage.innerText = ""
    sameusername.innerText = ""
    icon.innerHTML = `<input type="button" value="Upload Icon" onclick="document.querySelector('#selectedFile').click()">`
})

secondToLoginPage.addEventListener('click',function(){
    fullDisplay.classList.add("show-full-display");
    thirdFullDisplay.classList.remove("show-third-full-display");
    confirmButton.innerHTML = ""
})

username.addEventListener('input',async function(){
    let res = await fetch('/get-user');
    const usernamedata = await res.json();
    // console.log(usernamedata);
    for(let user of usernamedata){
         if(user.name === username.value && username.value){
            sameusername.innerText = 'This username has been used';
            return
        }
    }
    sameusername.innerText = "";
})
firstpassword.addEventListener('input',function(){
    wrongmessage.style = "color:#ff0000"
    if(firstpassword.value !== password.value){
        wrongmessage.innerText = "Password is unmatched"
    }else if(firstpassword.value === password.value){
        wrongmessage.innerText = "Password is matched"
        wrongmessage.style = "color:#0011ff"
    }
    if(!(password.value) || !(firstpassword.value)){
        wrongmessage.innerText = '';
    }
})

password.addEventListener('input',function(){
    wrongmessage.style = "color:#ff0000"
    if(firstpassword.value !== password.value){
        wrongmessage.innerText = "Password is unmatched"
    }else if(firstpassword.value === password.value){
        wrongmessage.innerText = "Password is matched"
        wrongmessage.style = "color:#0011ff"
    }
    if(!(password.value) || !(firstpassword.value)){
        wrongmessage.innerText = '';
    }
})

emailSignup.addEventListener('input',async function(){
    let res = await fetch('/get-user');
    const usernamedata = await res.json();
    for(let user of usernamedata){
        if(user.email === emailSignup.value && emailSignup.value){
            sameEmail.innerText = 'This email has been used';
           return
       }
   }
   sameEmail.innerText = "";
})

signupForm.addEventListener('submit',async(event)=>{
    event.preventDefault(); // To prevent the form from submitting synchronously
    
    const form = event.currentTarget;
    const user = new FormData();

    let res = await fetch('/get-user');
    const username = await res.json();
    // console.log(username);
    for(let user of username){
        if(user.name === form.username.value){
            warningButton.innerHTML = 'This username has been used'
            return;
        }
        if(user.email === form.email.value && form.email.value){
            warningButton.innerHTML = 'This email has been used'
            return;
        }
    }

    if(!(form.username.value)){
        warningButton.innerHTML = 'No username is typed in'
        return;
    }

    if(form.password.value === form.passwordAgain.value && form.password.value){
       
        user.append('username',form.username.value);
        user.append('password',form.password.value);
        user.append('email',form.email.value);
        if(form.image.files[0]){
            user.append('icon',form.image.files[0]);
        }
        if(form.birthday.value){
            user.append('birthday',form.birthday.value);
        }
        user.append('degree',form.experience.value);

        // create your form object with the form inputs
        await fetch('/signup',{
            method:"post",
            body:user
        });

        warningButton.innerHTML = 'Signup success';
    }else if(!form.password.value){
        warningButton.innerHTML = 'Empty password is typed';
    }else{
        warningButton.innerHTML = 'Unmatched password';
    }
    wrongmessage.innerText = '';
    form.reset();
});

hiddenUpload.addEventListener('input',function(){
    if(hiddenUpload.value){
        icon.innerHTML = `<img id='icon' onclick="document.querySelector('#selectedFile').click()" src="./image/${(hiddenUpload.value.substring(12))}">`;
        // $("#icon").draggable();
    }
})
sendPasswordEmail.addEventListener('click',async function(){
    if(email.value && email.value.includes("@")){
        const res = await fetch('/forget-password',{
            method:"post",
            headers:{
                "Content-Type":"application/json"
            },
            body:JSON.stringify({email:email.value})
        });
    
        const result = await res.json();
        if(result.success){
            confirmButton.innerHTML = 'The reset password link is sent to your email box';
        }else{
            confirmButton.innerHTML = 'The email address is not registered';
        }
    }else if(!(email.value.includes("@"))){
        confirmButton.innerHTML = 'This is not a valid email'
    }else{
        confirmButton.innerHTML = 'No email is typed'
    }  
})