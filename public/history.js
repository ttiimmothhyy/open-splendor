let userParam;
const params = new URLSearchParams(window.location.search)
for (const param of params) {
    userParam = param
    console.log(param)
}
getHistory(userParam[0])

const numberOneValue = document.querySelector("#number-1")
const numberTwoValue = document.querySelector("#number-2")
const numberThreeValue = document.querySelector("#number-3")
const numberFourValue = document.querySelector("#number-4")
const totalGameValue = document.querySelector("#total-game")
const winRateValue = document.querySelector("#winrate")
const userIcon = document.querySelector(".icon")
const userName = document.querySelector(".username")
const userExperience = document.querySelector(".experience")
const activityLog = document.querySelector("#activity-log")

let totalGame = 0
let numberOne = 0
let numberTwo = 0
let numberThree = 0
let numberFour = 0
let winrate;
let gameDateArray = []
let totalGameArray = []

function timeFormat(time) {
    let year = time.getFullYear()
    let month = time.getMonth()
    let day = time.getDay()
    if (month < 10) {
        month = "0" + month
    }
    if (day < 10) {
        day = "0" + day
    }
    return `${year}-${month}-${day}`
}

async function getHistory(name) {
    const userRes = await fetch("/users", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: JSON.stringify({ name: name })
    })
    const userData = await userRes.json()

    if (!userData) {
        console.log("user not exist")
        return
    }
    const userInformation = userData.userData


    userName.innerText = userInformation ? userInformation.name : "Player Not Exist"
    const userIconName = userInformation.icon
    const userExp = userInformation.experience
    if (userIconName) {
        userIcon.innerHTML = `
    <img class="user-icon" src="${userIconName}" alt="ICON">
    `
    }
    userExperience.innerHTML = `
    <div class="exp-value">
        ${userExp}/100
    </div>
    <div class="progress">
        <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: ${(userExp) ? userExp : 0}%"></div>
    </div>`


    const res = await fetch("/history", {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: JSON.stringify({ name: name })
    })
    const data = await res.json()

    if(!data){
        return
    }

    const history = data.history
    const username = data.username

    if(!history){
        return
    }

    totalGame = history.length
    let game = 0
    history.forEach(item => {
        game += 1
        let gameResult = item.result
        let gameDate = item["finish_time"]
        const li = document.createElement("li")
        li.classList.add("log")
        li.innerHTML = `<div>${game}</div>`
        for (let i = 0; i < 4; i++) {
            li.innerHTML += `
        <div>${gameResult[i] ? gameResult[i].name : ""}</div>
        `
        }

        li.innerHTML += `
        <div>${gameDate ? timeFormat(new Date(gameDate)) : ""}</div>
        `

        if(gameDate){
            gameDateArray.push(timeFormat(new Date(gameDate)))
            totalGameArray.push(game)
        }
        activityLog.appendChild(li)
        const rank = gameResult.findIndex(item => item.name == username)
        switch (rank) {
            case 0:
                numberOne += 1
                break;
            case 1:
                numberTwo += 1
                break;
            case 2:
                numberThree += 1
                break;
            case 3:
                numberFour += 1
                break;
        }
    })
    winrate = `${(numberOne * 100 / totalGame).toFixed(0)}%`

    totalGameValue.innerText = totalGame
    numberOneValue.innerText = numberOne
    numberTwoValue.innerText = numberTwo
    numberThreeValue.innerText = numberThree
    numberFourValue.innerText = numberFour
    winRateValue.innerText = winrate
}



let trace2=   {
  x: ['2013-10-04', '2013-11-04', '2013-12-04'],
  y: [1, 3, 6],
  type: 'scatter'
}

// let trace2 = {
//     x: gameDateArray,
//     y: totalGameArray,
//     mode: 'lines+markers',
//     name: 'Player total game',
// };

let data = [trace2];

let layout = {
    title: 'Improvement Stat (Coming Soon)',
    xaxis: {
        range: ['2013-09-01', '2013-12-31'],
        type: 'date'
    },
    yaxis: {
        title: 'Game'
    },
    plot_bgcolor: "2C2C2C",
    paper_bgcolor: "2C2C2C",
    showlegend: true,
    width: 1200,
    height: 500,
    font: {
        family: 'Courier New, monospace',
        size: 16,
        color: "#FFFFFF"
    },
};

let config = {responsive: true}
Plotly.newPlot('myDiv',data,layout,config);
