import {Client} from "pg";

export class HistoryService{
	constructor(private client:Client){}

	async hasHistory(username:string){
		return (await this.client.query(/* sql */ `select count(1) from history left outer join user_history_mapping
			on history.id = user_history_mapping.history_id
			left outer join users on users.id = user_history_mapping.user_id where users.name = $1`,
			[username]
		)).rows[0].count;
	}

	async userHistory(username:string){
		return (await this.client.query(/*sql*/ `select history.* from history left outer join user_history_mapping
			on history.id = user_history_mapping.history_id
			left outer join users on users.id = user_history_mapping.user_id where users.name = $1`,
			[username]
		)).rows
	}

	async getNumberOfPlayers(uuId:string){
		return (await this.client.query(/*sql*/`select num_of_players from rooms where uuid = $1`,[uuId]))
		.rows[0].num_of_players
	}

	async currentTurn(uuId:string){
		return (await this.client.query(/*sql*/`select current_turn from rooms where uuid = $1`,[uuId]))
		.rows[0].current_turn
	}

	async updateHistory(uuId:string,name:string,resultArray:string,timeSpent:string,numOfPlayers:number,currentTurn:number){
		await this.client.query(/*sql*/`insert into history (uuid,winner,result,time_spent,num_of_player,num_of_turn,finish_time)
			values ($1,$2,$3,$4,$5,$6,now())`,
			[uuId,name,resultArray,timeSpent,numOfPlayers,currentTurn]
		)
	}

	async updateMapping(uuId:string,username:string){
		const userId = (await this.client.query(/*sql*/`select history.id from history where uuid = $1`,[uuId])).rows[0].id;
		const historyId = (await this.client.query(/*sql*/`select id from users where users.name = $1`,[username])).rows[0].id;
		await this.client.query(/*sql*/`insert into user_history_mapping (user_id,history_id) values ($1,$2)`,[userId,historyId])
	}

	async totalHistory(){
		return (await this.client.query(/*sql*/`select count(1) from history left outer join user_history_mapping
			on history.id = user_history_mapping.history_id left outer join users on users.id = user_history_mapping.user_id`)
		).rows[0].count
	}

	async history(){
		return (await this.client.query(/*sql*/`select * from history`)).rows;
	}

	async users(){
		return (await this.client.query(/*sql*/`select name,experience from users`)).rows;
	}
}