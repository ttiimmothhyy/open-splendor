export let currentUser;
export let messagePanel1 = document.querySelector(".message-area-1");
export let messagePanel2 = document.querySelector(".message-area-2");
export let messagePanel3 = document.querySelector(".message-area-3");
export let messagePanel4 = document.querySelector(".message-area-4");
let roomGlobal = document.querySelector(".global");
let roomUser = document.querySelector('.user');
let roomRoom = document.querySelector(".room");
let roomPrivate = document.querySelector('.private');
let messageForm1 = document.querySelector(".message-form-1");
let messageForm2 = document.querySelector(".message-form-2");
let messageForm3 = document.querySelector(".message-form-3");
let messageForm4 = document.querySelector(".message-form-4");
export let userprofile = document.querySelector(".username");
let email = document.querySelector(".email");
let icon = document.querySelector(".icon");
let preference = document.querySelector(".preference-button");
let cross = document.querySelector(".imageflex");
let secondFullDisplay = document.querySelector(".second-full-display");
import {socket} from './playroom.js';
// Samuel :
// now lobby.html using both javbascript.js & lobby.js will call io.connecnt twice so serve will run io.connection twice


// let socket;

$(document).ready(function () {
    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });
})

roomGlobal.addEventListener("click", function () {
    messagePanel1.classList.add("chatroom-active");
    messagePanel2.classList.remove("chatroom-active");
    messagePanel3.classList.remove("chatroom-active");
    messagePanel4.classList.remove("chatroom-active");
    messageForm1.classList.add("message-active");
    messageForm2.classList.remove("message-active");
    messageForm3.classList.remove("message-active");
    messageForm4.classList.remove("message-active");
    roomGlobal.classList.add("title-active");
    roomUser.classList.remove("title-active");
    roomRoom.classList.remove("title-active");
    roomPrivate.classList.remove("title-active");
})

roomUser.addEventListener("click", async function () {
    messagePanel1.classList.remove("chatroom-active");
    messagePanel2.classList.add("chatroom-active");
    messagePanel3.classList.remove("chatroom-active");
    messagePanel4.classList.remove("chatroom-active");
    messageForm1.classList.remove("message-active");
    messageForm2.classList.add("message-active");
    messageForm3.classList.remove("message-active");
    messageForm4.classList.remove("message-active");
    roomGlobal.classList.remove("title-active");
    roomUser.classList.add("title-active");
    roomRoom.classList.remove("title-active");
    roomPrivate.classList.remove("title-active");

})

roomRoom.addEventListener("click", function () {
    messagePanel1.classList.remove("chatroom-active");
    messagePanel2.classList.remove("chatroom-active");
    messagePanel3.classList.add("chatroom-active");
    messagePanel4.classList.remove("chatroom-active");
    messageForm1.classList.remove("message-active");
    messageForm2.classList.remove("message-active");
    messageForm3.classList.add("message-active");
    messageForm4.classList.remove("message-active");
    roomGlobal.classList.remove("title-active");
    roomUser.classList.remove("title-active");
    roomRoom.classList.add("title-active");
    roomPrivate.classList.remove("title-active");
})

roomPrivate.addEventListener("click", function () {
    messagePanel1.classList.remove("chatroom-active");
    messagePanel2.classList.remove("chatroom-active");
    messagePanel3.classList.remove("chatroom-active");
    messagePanel4.classList.add("chatroom-active");
    messageForm1.classList.remove("message-active");
    messageForm2.classList.remove("message-active");
    messageForm3.classList.remove("message-active");
    messageForm4.classList.add("message-active");
    roomGlobal.classList.remove("title-active");
    roomUser.classList.remove("title-active");
    roomRoom.classList.remove("title-active");
    roomPrivate.classList.add("title-active");
})

preference.addEventListener("click", function () {
    secondFullDisplay.style = "display:block";
})
cross.addEventListener("click", function () {
    secondFullDisplay.style = "display:none";
})


document.querySelector('.message-form-1').addEventListener('submit', async (event) => {
    // 截咗原本個submit
    event.preventDefault();
    // 攞返form嘅element出嚟
    const form = event.currentTarget;
    // create your form object with the form inputs
    await fetch('/message/1', {
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({ content: form.content.value })
    });
    // Clear the form here
    form.reset();
})

document.querySelector('.message-form-2').addEventListener('submit', async (event) => {
    // 截咗原本個submit
    event.preventDefault();
    // 攞返form嘅element出嚟
    const form = event.currentTarget;

    // create your form object with the form inputs
    if (currentUser) {
        await fetch('/message/2', {
            method: "post",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({content:form.content.value})
        });
    }
    // Clear the form here
    form.reset();
})

document.querySelector('.message-form-3').addEventListener('submit',async(event)=>{
    // 截咗原本個submit
    event.preventDefault();
    // 攞返form嘅element出嚟
    const form = event.currentTarget;

    // create your form object with the form inputs
    await fetch('/message/3',{
        method: "POST",
        headers:{
            "Content-Type": "application/json"
        },
        body: JSON.stringify({content:form.content.value})
    });
    form.reset();
})

document.querySelector('.message-form-4').addEventListener('submit', async (event) => {
    // 截咗原本個submit
    event.preventDefault();
    // 攞返form嘅element出嚟
    const form = event.currentTarget;
    // create your form object with the form inputs

    await fetch('/message/4', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({content:form.content.value})
    });

    // Clear the form here
    form.reset();
})

async function chatRecordGlobal() {
    const res = await fetch('/chat-record-global');
    const result = await res.json();

    if (messagePanel1) {

        for (let message of result) {

            if(message.message.message){
                if (!(message.message.message[0].type === "Buffer")) {
                    let li = document.createElement("li")
                    li.innerText = `${message.name}: ${message.message.message}`;
                    messagePanel1.appendChild(li);
                } else {

                    let soundBuffer = new Uint8Array(message.message.message[0].data).buffer;
                    let arrayBuffer = [soundBuffer];


                    let audioData = new Blob(arrayBuffer,{'type':'audio/mp3'});
                    let li = document.createElement('li');

                    // Creating audio url with reference
                    // of created blob named 'audioData'
                    let audioSrc = window.URL.createObjectURL(audioData);

                    // Pass the audio url to the 2nd video tag
                    li.innerHTML = `${message.name}: <audio src="${audioSrc}" controls></audio>`
                    messagePanel1.appendChild(li);
                }
            }else{
                let li = document.createElement("li")
                li.innerText = `${message.name}: ${message.message.message}`;
                messagePanel1.appendChild(li);
            }
        }

    }
}

async function chatRecordUser() {
    const res = await fetch('/chat-record-user');
    const result = await res.json();

    if(messageForm2){
        for(let message of result){

            if(message.message.message){
                if(!(message.message.message[0].type === "Buffer")){
                    let li = document.createElement("li")
                    li.innerText = `${message.name}: ${message.message.message}`;
                    messagePanel2.appendChild(li);
                }else{

                    let soundBuffer = new Uint8Array(message.message.message[0].data).buffer;
                    let arrayBuffer = [soundBuffer];


                    let audioData = new Blob(arrayBuffer,{'type':'audio/mp3'});
                    let li = document.createElement('li');

                    // Creating audio url with reference
                    // of created blob named 'audioData'
                    let audioSrc = window.URL.createObjectURL(audioData);

                    // Pass the audio url to the 2nd video tag
                    li.innerHTML = `${message.name}: <audio src="${audioSrc}" controls></audio>`
                    messagePanel2.appendChild(li);
                }
            }else{
                let li = document.createElement("li")
                li.innerText = `${message.name}: ${message.message.message}`;
                messagePanel2.appendChild(li);
            }

        }
    }
}

async function chatRecordRoom(){
    const res = await fetch('/chat-record-room');
    const result = await res.json();

    if(messageForm3){
        for(let message of result){
            if(message.message.message){
                if(!(message.message.message[0].type === "Buffer")){
                    let li = document.createElement("li")
                    li.innerText = `${message.name}: ${message.message.message}`;
                    messagePanel3.appendChild(li);
                }else{
                    let soundBuffer = new Uint8Array(message.message.message[0].data).buffer;
                    let arrayBuffer = [soundBuffer];
                    let audioData = new Blob(arrayBuffer,{'type':'audio/mp3'});
                    let li = document.createElement('li');
                    // Creating audio url with reference
                    // of created blob named 'audioData'
                    let audioSrc = window.URL.createObjectURL(audioData);
                    // Pass the audio url to the 2nd video tag
                    li.innerHTML = `${message.name}: <audio src="${audioSrc}" controls></audio>`
                    messagePanel3.appendChild(li);
                }
            }else{
                let li = document.createElement("li")
                li.innerText = `${message.name}: ${message.message.message}`;
                messagePanel3.appendChild(li);
            }
        }
    }
}

async function chatRecordFriend() {
    const res = await fetch('/chat-record-friend');
    const result = await res.json();

    if(messageForm4){
        for(let message of result){

            if(message.message.message){
                if (!(message.message.message.includes('/uploads/'))){
                    let li = document.createElement("li")
                    li.innerText = `${message.name}: ${message.message.message}`;
                    messagePanel4.appendChild(li);
                }else{
                    let li = document.createElement('li');
                    // Pass the audio url to the 2nd video tag
                    li.innerHTML = `${message.name}: <audio src="${message.message.message}" controls></audio>`
                    messagePanel4.appendChild(li);
                }
            }else{
                let li = document.createElement("li")
                    li.innerText = `${message.name}: ${message.message.message}`;
                    messagePanel4.appendChild(li);
            }

        }
    }
}

async function getCurrentUser(){
    const res = await fetch('/current-user');
    const result = await res.json();


    if(result.success){
        currentUser = result.user;
        userprofile.innerText = currentUser.name;
        if (currentUser.email) {
            let oldText = `@${currentUser.email}`;
            let newText;
            if(oldText.length > 24){
                newText = oldText.substring(0, 24) + '\n' + oldText.substring(24, oldText.length);
            }else{
                newText = oldText;
            }
            email.innerText = newText;
        }else{
            email.innerText = `@${currentUser.name}`;
        }
        if(currentUser.icon){
            icon.innerHTML = `<img src="${currentUser.icon}" id="icon-picture">`;
        }else{
            icon.innerHTML = '<i class="fas fa-street-view"></i>';
        }
    }
}



async function recording(){
    let audioIN = {audio:true};
    const mediaStreamObj = await navigator.mediaDevices.getUserMedia(audioIN);

    let startGlobal = document.querySelector("#microphone-1");
    let startRecodingGlobal = document.querySelector(".start-recording-1");
    let microphoneGlobalClick = true;
    let mediaRecorderGlobal = new MediaRecorder(mediaStreamObj);

    let startUser = document.querySelector("#microphone-2");
    let startRecodingUser = document.querySelector(".start-recording-2");
    let microphoneUserClick = true;
    let mediaRecorderUser = new MediaRecorder(mediaStreamObj);

    let startRoom = document.querySelector("#microphone-3");
    let startRecodingRoom = document.querySelector(".start-recording-3");
    let microphoneRoomClick = true;
    let mediaRecorderRoom = new MediaRecorder(mediaStreamObj);

    let startFriend = document.querySelector("#microphone-4");
    let startRecodingFriend = document.querySelector(".start-recording-4");
    let microphoneFriendClick = true;
    let mediaRecorderFriend = new MediaRecorder(mediaStreamObj);

    startGlobal.addEventListener('click',function(){
        if(microphoneGlobalClick){
            let changeColor = true
            mediaRecorderGlobal.start();
            microphoneGlobalClick = false;
            startGlobal.classList.add("microphone-active");
            startRecodingGlobal.innerHTML = '<i class="far fa-comment-dots"></i>';
            setInterval(function(){
                if(changeColor){
                    startRecodingGlobal.style = "color:#ffffff";
                    changeColor = false;
                }else{
                    startRecodingGlobal.style = "color:#1d1f20";
                    changeColor = true;
                }
            },800)
        }else{
            mediaRecorderGlobal.stop();
            microphoneGlobalClick = true;
            startGlobal.classList.remove("microphone-active");
            startRecodingGlobal.innerHTML = '';
        }
    })

    startUser.addEventListener('click',function(){
        if(microphoneUserClick){
            let changeColor = true
            mediaRecorderUser.start();
            microphoneUserClick = false;
            startUser.classList.add("microphone-active");
            startRecodingUser.innerHTML = '<i class="far fa-comment-dots"></i>';
            setInterval(function () {
                if(changeColor){
                    startRecodingUser.style = "color:#ffffff";
                    changeColor = false;
                }else{
                    startRecodingUser.style = "color:#1d1f20";
                    changeColor = true;
                }
            },800)
        }else{
            mediaRecorderUser.stop();
            microphoneUserClick = true;
            startUser.classList.remove("microphone-active");
            startRecodingUser.innerHTML = '';
        }
    })

    startRoom.addEventListener('click',function(){
        if(microphoneRoomClick){
            let changeColor = true
            mediaRecorderRoom.start();
            microphoneRoomClick = false;
            startRoom.classList.add("microphone-active");
            startRecodingRoom.innerHTML = '<i class="far fa-comment-dots"></i>';
            setInterval(function(){
                if(changeColor){
                    startRecodingRoom.style = "color:#ffffff";
                    changeColor = false;
                }else{
                    startRecodingRoom.style = "color:#1d1f20";
                    changeColor = true;
                }
            },800)
        }else{
            mediaRecorderRoom.stop();
            microphoneRoomClick = true;
            startRoom.classList.remove("microphone-active");
            startRecodingRoom.innerHTML = '';
        }
    })

    startFriend.addEventListener('click',function(){
        if(microphoneFriendClick){
            let changeColor = true
            mediaRecorderFriend.start();
            microphoneFriendClick = false;
            startFriend.classList.add("microphone-active");
            startRecodingFriend.innerHTML = '<i class="far fa-comment-dots"></i>';
            setInterval(function(){
                if(changeColor){
                    startRecodingFriend.style = "color:#ffffff";
                    changeColor = false;
                }else{
                    startRecodingFriend.style = "color:#1d1f20";
                    changeColor = true;
                }
            },800)
        }else{
            mediaRecorderFriend.stop();
            microphoneFriendClick = true;
            startFriend.classList.remove("microphone-active");
            startRecodingFriend.innerHTML = '';
        }
    })

    let dataArray = [];
    mediaRecorderGlobal.ondataavailable = function(event){
        dataArray.push(event.data);
    }
    // Convert the audio data in to blob
    // after stopping the recording
    mediaRecorderGlobal.onstop = async function () {
        // blob of type mp3

        socket.emit("sound-global",dataArray);
        // After fill up the chunk
        // array make it empty
        dataArray = [];
    }

    mediaRecorderUser.ondataavailable = function(event){
        dataArray.push(event.data);
    }
    mediaRecorderUser.onstop = async function () {
        // blob of type mp3

        socket.emit("sound-user", dataArray);
        // After fill up the chunk
        // array make it empty
        dataArray = [];
    }

    mediaRecorderRoom.ondataavailable = function(event){
        dataArray.push(event.data);
    }
    mediaRecorderRoom.onstop = async function(){
        // blob of type mp3

        socket.emit("sound-room", dataArray);
        // After fill up the chunk
        // array make it empty
        dataArray = [];
    }

    mediaRecorderFriend.ondataavailable = function(event){
        dataArray.push(event.data);
    }
    mediaRecorderFriend.onstop = async function(){
        // blob of type mp3

        socket.emit("sound-friend",dataArray);
        // After fill up the chunk
        // array make it empty
        dataArray = [];
    }

    socket.on("sound-transverse-global",(soundData)=>{

        let audioData = new Blob(soundData.message,{'type':'audio/mp3'});
        let li = document.createElement('li');

        // Creating audio url with reference
        // of created blob named 'audioData'
        let audioSrc = window.URL.createObjectURL(audioData);

        // Pass the audio url to the 2nd video tag
        li.innerHTML = `${soundData.user}: <audio src="${audioSrc}" controls></audio>`
        messagePanel1.appendChild(li);
    })

    socket.on("sound-transverse-user",(soundData)=>{

        let audioData = new Blob(soundData.message,{'type':'audio/mp3'});
        let li = document.createElement('li');

        // Creating audio url with reference
        // of created blob named 'audioData'
        let audioSrc = window.URL.createObjectURL(audioData);

        // Pass the audio url to the 2nd video tag
        li.innerHTML = `${soundData.user}: <audio src="${audioSrc}" controls></audio>`
        messagePanel2.appendChild(li);
    })

    socket.on("sound-transverse-room",(soundData)=>{

        let audioData = new Blob(soundData.message,{'type':'audio/mp3'});
        let li = document.createElement('li');

        // Creating audio url with reference
        // of created blob named 'audioData'
        let audioSrc = window.URL.createObjectURL(audioData);

        // Pass the audio url to the 2nd video tag
        li.innerHTML = `${soundData.user}: <audio src="${audioSrc}" controls></audio>`
        messagePanel3.appendChild(li);
    })

    socket.on("sound-transverse-friend",async(soundData)=>{

        let audioData = new Blob(soundData.message,{'type':'audio/mp3'});
        let li = document.createElement('li');

        // Creating audio url with reference
        // of created blob named 'audioData'
        let audioSrc = window.URL.createObjectURL(audioData);
        // Pass the audio url to the 2nd video tag
        li.innerHTML = `${soundData.user}: <audio src="${audioSrc}" controls></audio>`
        messagePanel4.appendChild(li);

        const message = new FormData();

        const blob = new Blob(soundData.message);
        const sound = new File([blob],'filename.mp3');

        message.append('name', soundData.user);
        message.append('content',sound);
        message.append('belong',userprofile.innerText);
        await fetch("/store-voice-friend",{
            method: "post",
            body:message
        })
    })
}


window.onload = async function () {
    await getCurrentUser();
    await chatRecordGlobal();
    await chatRecordUser();
    await chatRecordRoom();
    await chatRecordFriend();
    await recording();
}

