import { gameIsStart, currentPlayer, dataTransfer, endTurn, socket } from "./playroom.js"
import { playerArray } from "./playroom.js"
import {bank} from "./table.js"
import {getSound} from "./sound.js"

// Get resources logic / rules
let virtualToken = { red: 0, green: 0, blue: 0, brown: 0, white: 0, gold: 0 }
const reduceVirtualToken = document.querySelectorAll(".reduce-virtual-token")

const bankToken = document.querySelectorAll(".bank-token .inner-circle")
const getResourceResetButton = document.querySelector("#get-resource-reset")
const getResourceComfirmButton = document.querySelector("#get-resource-confirm-button")
bankToken.forEach((item, index) => {
    item.addEventListener("click", async() => {
        // console.log(virtualToken)
        if (!gameIsStart) {
            // console.log("game not start yet")
            return
        }
        if (item.querySelector(".remaining-token").innerText == 0) {
            // console.log("there is no more resource")
            return
        }
        if (index == 5) {
            // console.log("you can't get gold token")
            return
        }
        if (resourceGetValidation(virtualToken)) {
            let counter = 0
            for (let key in virtualToken) {
                counter += virtualToken[key]
            }
            if (item.querySelector(".remaining-token").innerText < 3 && virtualToken[Object.keys(virtualToken)[index]] == 1) {
                // console.log("you can't get two specific token when resource is less then 4")
                return
            }
            if (counter == 2 && virtualToken[Object.keys(virtualToken)[index]] == 1) { //virtualToken[Object.keys(virtualToken)[index]] is current get token
                // console.log("you can only get one token from three different token type respectively")
                return
            }
            await getSound("clickToken","effect")
            virtualToken[Object.keys(virtualToken)[index]] += 1
            item.querySelector(".remaining-token").innerText -= 1
            item.parentNode.querySelector(".virtual-token").innerText = `+${virtualToken[Object.keys(virtualToken)[index]]}`
            resourceGetValidation(virtualToken)

            // console.log("hihi")
            // console.log(index)
            reduceVirtualToken[index].innerText = "-"
        }else{
            return
        }
        getResourceResetButton.disabled = false;
        getResourceComfirmButton.disabled = false;
        // console.log("select token: ", virtualToken)
    })
})
reduceVirtualToken.forEach((item,index)=>{
    item.addEventListener("click",async()=>{
        console.log(item.parentNode.querySelector(".remaining-token"))
        if(virtualToken[Object.keys(virtualToken)[index]] > 0){
            await getSound("clickToken","effect")
            virtualToken[Object.keys(virtualToken)[index]] -= 1
            item.parentNode.querySelector(".remaining-token").innerText = Number(item.parentNode.querySelector(".remaining-token").innerText) + 1
            item.parentNode.querySelector(".virtual-token").innerText = `+${virtualToken[Object.keys(virtualToken)[index]]}`
            if(virtualToken[Object.keys(virtualToken)[index]] == 0 ){
                item.parentNode.querySelector(".virtual-token").innerText = ""
                reduceVirtualToken[index].innerText = ""
            }

            if(!virtualToeknSelected()){
                getResourceResetButton.disabled = true;
                getResourceComfirmButton.disabled = true;
            }
        }
    })
})
function virtualToeknSelected(){
    let isSelected = false
    Object.keys(virtualToken).forEach(item=>{
        // console.log(virtualToken[item])
        if(virtualToken[item]!=0){
            // console.log("hi")
            isSelected = true
        }
    })
    return isSelected
}

function resourceGetValidation(tokenGetObject) {
    let counter = 0
    for (let key in tokenGetObject) {
        if (tokenGetObject[key] >= 2) {
            // console.log("you can get only two token for single resource type")
            return false
        }
        counter += tokenGetObject[key]
    }
    if (counter >= 3) {
        // console.log("you can get only three token for maximum")
        return false
    }
    if (playerResourceSumLimit(currentPlayer,virtualToken)>=10) {
        // console.log("you can't get more than 10 token")
        return false
    }
    return true
}

function playerResourceSumLimit(currentPlayer,virtualToken) {
    let sum = 0
    for (let key in playerArray[currentPlayer].resource) {
        sum += playerArray[currentPlayer].resource[key] +virtualToken[key]
    }
    return sum
}
getResourceResetButton.addEventListener("click", resetGetToken)
export function resetGetToken() {
    getResourceResetButton.disabled = true;
    getResourceComfirmButton.disabled = true;
    bankToken.forEach((item, index) => {
        reduceVirtualToken[index].innerText = ""
        item.parentNode.querySelector(".virtual-token").innerText = ""
        item.querySelector(".remaining-token").innerText = bank.resource[Object.keys(bank.resource)[index]]
        virtualToken[Object.keys(virtualToken)[index]] = 0
    })
}
getResourceComfirmButton.addEventListener("click", async () => {
    getResourceResetButton.disabled = true;
    getResourceComfirmButton.disabled = true;
    // console.log("get token from bank")
    // console.log(currentPlayer)
    playerArray[currentPlayer].getResource(virtualToken)

    await dataTransfer()
    socket.emit("get-token",{player:currentPlayer,token:virtualToken})
    resetGetToken()
    endTurn()
})


