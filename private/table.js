export class GameTable {
    constructor() {
        this.novelCardDeck = []
        this.novelShownCard = []
        this.highLevelDeck = []
        this.midLevelDeck = []
        this.lowLevelDeck = []
        this.highLevelShownDeck = []
        this.midLevelShownDeck = []
        this.lowLevelShownDeck = []
    }

    leaveCardFromDeck(card) {
        if (!card) {
            alert("no card in deck")
            return false
        }
        switch (card.tier) {
            case "LL":
                this.lowLevelDeck.pop();
                break;
            case "ML":
                this.midLevelDeck.pop();
                break;
            case "HL":
                this.highLevelDeck.pop();
                break
            default:
                // console.log(`this card not in deck`)
        }
        return true
    }

    leaveCardFromShownDeck(card) {
        // console.log("now remove card from deck shown card & add from deck")
        if (!card) {
            alert("no card in shownDeck")
            return false
        }
        switch (card.tier) {
            case "LL":
                const index1 = this.lowLevelShownDeck.findIndex(item => item.id == card.id)
                if (index1 >= 0) {
                    if (this.lowLevelDeck.length == 0) {
                        this.lowLevelShownDeck.splice(index1, 1)
                    } else {
                        this.lowLevelShownDeck.splice(index1, 1, this.lowLevelDeck.pop())
                    }
                    break
                }
            case "ML":
                const index2 = this.midLevelShownDeck.findIndex(item => item.id == card.id)
                if (index2 >= 0) {
                    if (this.midLevelDeck.length == 0) {
                        this.midLevelShownDeck.splice(index2, 1)
                    } else {
                        this.midLevelShownDeck.splice(index2, 1, this.midLevelDeck.pop())
                    }
                    break
                }
            case "HL":
                const index3 = this.highLevelShownDeck.findIndex(item => item.id == card.id)
                if (index3 >= 0) {
                    if(this.highLevelDeck.length === 0){
                        this.highLevelShownDeck.splice(index3, 1)
                    }else{
                        this.highLevelShownDeck.splice(index3, 1, this.highLevelDeck.pop())
                    }
                    break
                }
            default:
                // console.log(`this card not in shown deck`)
        }
        return true
    }
}

export class BankResource {
    constructor(resource) {
        this.resource = resource //Initialize base on player number
    }
    add(resourceReturn) {
        for (let key in resourceReturn) {
            this.resource[key] += resourceReturn[key]
        }
    }
    reduce(resourceProvide) { //****need to set a guard to prevent <0 */
        for (let key in resourceProvide) {
            this.resource[key] -= resourceProvide[key]
        }
        // console.log("bank resource : ", this.resource)
    }
    checkStorageEnought(resourceProvide) {
        let cloneResource = JSON.parse(JSON.stringify(this.resource))
        for (let key in resourceProvide) {
            cloneResource[key] -= resourceProvide[key]
            if (cloneResource[key] < 0) {
                console.log("Bank can't provide resource")
                return false
            }
        }
        return true
    }
}

export const gameTable = new GameTable()
const bankResource = { red: 7, green: 7, blue: 7, brown: 7, white: 7, gold: 7 }
export const bank = new BankResource(bankResource)