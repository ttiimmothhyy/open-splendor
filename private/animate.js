export function addAnimation() {
    // store the x,y coordinates of the target  
    let elements = document.querySelector(".remaining-token-container")
    let target = document.querySelectorAll("#player1 .my-token")[3]

    console.log(elements)
    console.log(target)
    target.classList.add("relative")
    let movingObject = document.createElement("div")
    let virtualTarget = document.createElement("div")
    virtualTarget.classList.add("absolute")
    virtualTarget.classList.add("virtualTarget")
    movingObject.classList.add("absolute")
    movingObject.classList.add("moving-obj")
    target.appendChild(virtualTarget)
    target.appendChild(movingObject)

    var xT = virtualTarget.offsetLeft;
    var yT = virtualTarget.offsetTop;
    var xE = movingObject.offsetLeft;
    var yE = movingObject.offsetTop;

    // movingObject.style.transition = "10s"

    movingObject.style.left = xE + 'px';
    movingObject.style.top = yE + 'px';
    setTimeout(()=>{
        movingObject.style.left = xT + 'px';
        movingObject.style.top = yT + 'px';
    },20)
    // set their position to the target position
    // the animation is a simple css transition

}