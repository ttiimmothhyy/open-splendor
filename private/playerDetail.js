import {playerArray} from "./playroom.js"
import {numberOfPlayer,isYourTurn} from "./playroom.js"
import {modifybuyCardPosition} from "./buyCard.js"
import {getSound} from "./sound.js"
const playerDetail = document.querySelectorAll(".card-check")
const playerInformationContainer = document.querySelector("#player-container")
const playerInformationBox = document.querySelectorAll("#informationBox")
const closePlayerInformationBoxButton = document.querySelectorAll(".close-button")
const playerCardInformationContainer = document.querySelectorAll(".player-card-container")

export function playerCardDetailsCreate() {
    playerDetail.forEach((button, index) => {
        if (index >= numberOfPlayer) {
            return
        }
        button.addEventListener("click", () => {

            playerInformationBox[index].style.width = "100%"
            closePlayerInformationBoxButton[index].style.visibility = "visible"

            playerArray[index].deck.forEach(item => {
                let boughtCard = document.createElement("div")
                boughtCard.setAttribute("data-id", item.id)
                boughtCard.classList.add("bought-card-size")
                boughtCard.classList.add("hold-card")

                let costItemDiv = "";
                for (let key in item.cost) {
                    if (item.cost[key] != 0) {
                        costItemDiv += `
                        <div class="${key} cost-circle">
                            ${item.cost[key]}
                        </div>`
                    }
                }
                boughtCard.innerHTML = `
                <div class="bought-card">
                    <div class="${item.production} bought-card-cover">
                        <div class="card-top">
                            <div class="score">${item.score}</div>
                            <div class="production ${item.production}"></div>
                        </div>
                        <div class="card-bottom">
                            <div class="cost">
                                ${costItemDiv}
                            </div>
                        </div>
                    </div>
                </div>
                `
                playerCardInformationContainer[index].appendChild(boughtCard)
            })
        })
        closePlayerInformationBoxButton[index].addEventListener("click", () => {
            closePlayerInformationBoxButton[index].style.visibility = "hidden"
            playerInformationBox[index].style.width="0%"
            playerCardInformationContainer[index].innerHTML = ""
        })
    })
}

export function playerHoldCardDetailsCreate(data) {
    // console.log("playerHoldCardDetailsCreate check click time")
    // console.log("You are Player : ", data + 1)
    const playerOder = data + 1
    document.querySelector(`.player-name`).innerHTML = `Player ${playerOder}`

    const playerHoldbutton = document.querySelector(`.player${playerOder}-hold`)
    playerHoldbutton.style.cursor = "pointer"
    playerHoldbutton.addEventListener("click", () => {
        playerInformationBox[playerOder-1].style.width = "100%"
        closePlayerInformationBoxButton[playerOder-1].style.visibility = "visible"

        playerArray[playerOder-1].holdDeck.forEach(item => {
            let holdCard = document.createElement("div")
            holdCard.setAttribute("data-id", item.id)
            holdCard.classList.add("bought-card-size")
            holdCard.classList.add("hold-card")

            let costItemDiv = "";
            for (let key in item.cost) {
                if (item.cost[key] != 0) {
                    costItemDiv += `
                    <div class="${key} cost-circle">
                        ${item.cost[key]}
                    </div>`
                }
            }
            holdCard.innerHTML = /*html*/`
            <div class="bought-card">
                <div class="${item.production} bought-card-cover">
                    <div class="card-top">
                        <div class="score">${item.score}</div>
                        <div class="production ${item.production}"></div>
                    </div>
                    <div class="card-bottom">
                        <div class="cost">
                            ${costItemDiv}
                        </div>
                    </div>
                </div>
            </div>`
            playerCardInformationContainer[playerOder-1].appendChild(holdCard)
        })
        document.querySelectorAll(".hold-card").forEach((cardInHoldBox,index)=>{
            cardInHoldBox.addEventListener("click",async()=>{
                // console.log("click card : ",isYourTurn)
                if(isYourTurn){
                    await getSound("click") //click card sound effect
                    // console.log("click hold card")
                    modifyselectedCardForBuy(playerArray[playerOder-1].holdDeck[index]) //the immutable exported module values.
                    modifybuyCardPosition("hold")
                    closeInformationButton()
                    buyCardBoxCreate()
                }
            })
        })
    })
    closePlayerInformationBoxButton[playerOder-1].addEventListener("click", closeInformationButton)
    function closeInformationButton(){
        closePlayerInformationBoxButton[playerOder-1].style.visibility = "hidden"
        playerInformationBox[playerOder-1].style.width = "0%"
        playerCardInformationContainer[playerOder-1].innerHTML = ""
    }
}
