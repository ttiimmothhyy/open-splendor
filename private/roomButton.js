import {startGameButton,readyButton} from "./playroom.js"
import {captureStartButton} from "./gameCapture.js"
import {saveGameButton} from "./saveGame.js"
import {getSound} from "./sound.js"
import {quitRoomButton} from"./quitRoom.js"

startGameButton.addEventListener("mouseover",async()=>{
    await getSound("startGameButton", "effect")
})
readyButton.addEventListener("mouseover",async()=>{
    await getSound("startGameButton", "effect")
})
captureStartButton.addEventListener("mouseover",async()=>{
    await getSound("startGameButton", "effect")
})
saveGameButton.addEventListener("mouseover",async()=>{
    await getSound("startGameButton", "effect")
})
quitRoomButton.addEventListener("mouseover",async()=>{
    await getSound("startGameButton", "effect")
})

export function messageBox(message,choice){
    let messageBox = document.querySelector("#message-box");
    messageBox.style.display = "block";
    messageBox.parentNode.style.zIndex = "3";
    document.querySelector("#message-box .meassage").innerText = `${message}`;
    function yes(){
        choice(true);
        document.querySelector("#message-box .confirm-button").removeEventListener("click",yes);
        document.querySelector("#message-box .reject-button").removeEventListener("click", no);
    }
    function no(){
        choice(false);
        document.querySelector("#message-box .confirm-button").removeEventListener("click",yes);
        document.querySelector("#message-box .reject-button").removeEventListener("click", no);
    }
    document.querySelector("#message-box .confirm-button").addEventListener("click",yes);
    document.querySelector("#message-box .reject-button").addEventListener("click", no);
}