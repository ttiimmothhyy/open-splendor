const bgmSoundGain = document.querySelector("#bgm-volume")
const effectSoundGain = document.querySelector("#effect-volume")

const audioContext = new AudioContext({latencyHint:0,sampleRate:44100}) //,sampleRate:20000}
const effectGainNode = new GainNode(audioContext,{gain:effectSoundGain.value})
const bgmGainNode = new GainNode(audioContext,{gain:bgmSoundGain.value})


setupEvenetListeners()
function setupEvenetListeners(){
    effectSoundGain.addEventListener("input",e=>{
        const value = parseFloat(e.target.value)
        effectGainNode.gain.setValueAtTime(value, audioContext.currentTime)
    })
    bgmSoundGain.addEventListener("input",e=>{
        const value = parseFloat(e.target.value)
        bgmGainNode.gain.setValueAtTime(value, audioContext.currentTime)
    })
}

export async function getSound(sound,use="effect"){
    const res = await fetch(`/sound?name=${sound}`)
    const soundBuffer = await res.arrayBuffer()
    const getTokenBuffer = await audioContext.decodeAudioData(soundBuffer)

    const soundSource = audioContext.createBufferSource();
    soundSource.buffer = getTokenBuffer
  
    if(use=="bgm"){
        // var loopingEnabled = soundSource.loop;
        soundSource.loop = true ;
        soundSource.connect(bgmGainNode)
        bgmGainNode.connect(audioContext.destination)
    }else if(use=="effect"){   
        soundSource.connect(effectGainNode)
        effectGainNode.connect(audioContext.destination)
    }
    soundSource.start()
}