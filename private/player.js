import {gameTable,bank} from "./table.js"
export class Player {
    constructor(name,id) {
        this.name=name
        this.id=id
        this.resource = { red: 0, green: 0, blue: 0, brown: 0, white: 0, gold: 0 }
        this.score = 0
        this.deck = []
        this.holdDeck = []
        this.novelCardDeck = []
    }

    //cardPosition = "hold" || "table"
    buyCard(card,cardPosition, tokenSelected) {
        for(let key in tokenSelected){ 
            this.resource[key] -= tokenSelected[key];
        }
        console.log("after reduced resource : ",this.resource)

        bank.add(tokenSelected) 
        //(2) add card to card deck
        this.deck.push(card)
        //(3) remove card
        if(cardPosition=="table"){
            
            gameTable.leaveCardFromShownDeck(card)
        }else{
            const removeCardIndex = this.holdDeck.findIndex(item=>item.id==card.id)
            this.holdDeck.splice(removeCardIndex, 1)
        }
        
        //(4) add score
        this.score += card.score 
        //(5) check novelCard from table
        let clonedGameTablenovelShownCard = JSON.parse(JSON.stringify(gameTable.novelShownCard))
        clonedGameTablenovelShownCard.forEach((novelCard,index)=>{
            if(this.novelCardCheck(novelCard)){
                this.novelCardDeck.push(novelCard);
                this.score += novelCard.score;
                gameTable.novelShownCard.splice(index,1);
                console.log(gameTable.novelShownCard);
            }
        })
    }

    getResource(resourceGet) { //get logic put in class game so that resourceGet must be valid
        for(let key in resourceGet){
            this.resource[key] += resourceGet[key];
        }
        bank.reduce(resourceGet);;
        return true;
    }

    holdCard(card,locationIndex){ //location can be -1:from deck or  0-4:from shown deck
        if(!(this.holdDeck.length < 3)){ 
            alert("player can't hold more than three card");
            return false;
        }
        
        if(locationIndex >= 0){
            if(!gameTable.leaveCardFromShownDeck(card)){
                return false;
            }
        }else{
            if(!gameTable.leaveCardFromDeck(card)){
                return false;
            }
        }
        this.holdDeck.push(card);
        if (bank.checkStorageEnought({ gold: 1 })) {
            this.getResource({gold:1});
        }
        return true;
    }

    totalProduction(){
        let productionOrigin = { red: 0, green: 0, blue: 0, brown: 0, white: 0 }
        let sumProduction = this.deck.reduce((prev, curr) => {
                productionOrigin[curr.production] += 1
            return productionOrigin
        }, productionOrigin)
        return sumProduction
    }

    novelCardCheck(novelCard){
        for (let key in novelCard.productionRequirement) {
            if (this.totalProduction()[key] < novelCard.productionRequirement[key]) {
                return false
            }
        }
        return true
    }
    checkGameEnd(){
        return this.score >= 3
    }
}