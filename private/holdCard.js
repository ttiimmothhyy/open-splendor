import {gameTable} from "./table.js"
import {currentPlayer,dataTransfer,endTurn,socket} from "./playroom.js"
import {playerArray} from "./playroom.js"

let card = document.querySelectorAll(".shown-card");
let deck = document.querySelectorAll(".deck")
let draggedCard;
let draggedCardType;
function handleDragStart(e) {
    e.target.style.opacity = '0.4'
    draggedCard = e.target
    draggedCard.classList.remove("animate__animated")
    draggedCard.classList.remove("animate__rubberBand")
}

function handleDragEnd(e) {
   e.target.style.opacity = '1'
}

function handleDragOver(e) {
    e.preventDefault();
    e.target.classList.add("over")
}

function handleDragEnter(e) {
    e.preventDefault();
    e.target.classList.add("over")
}

function handleDragLeave(e) {
    e.preventDefault();
    e.target.classList.remove("over")
}

async function handleDragDrop(e) {
    e.target.classList.remove("over")
    e.target.classList.remove("animate__animated")
    e.target.classList.remove("animate__rubberBand")

    draggedCard.style.opacity = "1"

    if (holdCardHandling(draggedCard)) {
        await dataTransfer()
        socket.emit("hold-card",{player:currentPlayer,cardHoldType:draggedCardType})

        endTurn()
    }
}
export function addHoldCardOn() {

    card.forEach(item => {
        item.addEventListener("dragstart", handleDragStart, false)
        item.addEventListener("dragend",handleDragEnd,false)
    })
    deck.forEach(item=>{
        item.addEventListener("dragstart", handleDragStart, false)
        item.addEventListener("dragend",handleDragEnd,false)
    })

    let holdTarget = document.querySelectorAll(".hold-card-box")
    // console.log(holdTarget)
    holdTarget[currentPlayer].addEventListener('dragenter', handleDragEnter, false);
    holdTarget[currentPlayer].addEventListener('dragleave', handleDragLeave, false);
    holdTarget[currentPlayer].addEventListener('dragover', handleDragOver, false);
    holdTarget[currentPlayer].addEventListener('drop', handleDragDrop, false);
}

function holdCardHandling(draggedCard) {
    // console.log("put down card")
    const draggedCardData = draggedCard.getAttribute("data-id")
    draggedCardType = { //-1 mean it is from deck ,  0 -4 mean it is from shown deck
        "tier": draggedCardData.split("-")[0],
        "shownCardArrayIndex": Number(draggedCardData.split("-")[1]) - 1
    }
    // console.log(draggedCardType)
    // console.log("deck card: ",gameTable.highLevelDeck)
    let cardForHold;
    let successHold =false
    switch (draggedCardType.tier) {
        case "LL":
            if(draggedCardType.shownCardArrayIndex==-1){
                cardForHold = gameTable.lowLevelDeck[gameTable.lowLevelDeck.length-1]
            }else{
                cardForHold = gameTable.lowLevelShownDeck[draggedCardType.shownCardArrayIndex]
            }
            successHold = playerArray[currentPlayer].holdCard(cardForHold,draggedCardType.shownCardArrayIndex)
            break
        case "ML":
            if(draggedCardType.shownCardArrayIndex==-1){
                cardForHold = gameTable.midLevelDeck[gameTable.midLevelDeck.length-1]
            }else{
                cardForHold = gameTable.midLevelShownDeck[draggedCardType.shownCardArrayIndex]
            }
            successHold = playerArray[currentPlayer].holdCard(cardForHold,draggedCardType.shownCardArrayIndex)
            break
        case "HL":
            if(draggedCardType.shownCardArrayIndex==-1){
                cardForHold = gameTable.highLevelDeck[gameTable.highLevelDeck.length-1]
            }else{
                cardForHold = gameTable.highLevelShownDeck[draggedCardType.shownCardArrayIndex]
            }
            successHold = playerArray[currentPlayer].holdCard(cardForHold,draggedCardType.shownCardArrayIndex)
            break
    }
    return successHold
}
