import { messageBox } from "./roomButton.js"

export let quitRoomButton = document.querySelector(".quit-room-button")
quitRoomButton.addEventListener("click", quitRoom)
function quitRoom() {
    messageBox("You can't join this room again, still Quit ?", (choice) => {
        if (choice) {
            console.log("quit")
            window.location.href = "/quit_room"
        } else {
            document.querySelector("#message-box").style.display = "none"
            document.querySelector("#message-box").parentNode.style.zIndex = "0"
        }
    })
}

